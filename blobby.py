######################################################################
##
## Module      :  blobby
## Copyright   :  (c) hokum
## License     :  GPL3
##
## Maintainer  :
## Stability   :  experimental
## Portability :
##
## |
#######################################################################

bl_info = {
    "name": "Blobby",
    "description": "Generate blobby figures",
    "author": "Hokum",
    "version": (0, 1),
    "blender": (2, 78, 0),
    "location": "View3D > Add > Mesh",
    "warning": "", # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "support": "COMMUNITY",
    "category": "Add Mesh"
    }


import bpy
import json
import io
#import math
from os import system
import mathutils
from math import radians,degrees

from bpy.props import *


resultingObjects={}


#
#  Run render only from script! NOT GUI (contexts won't match)
#
def gen_blob_render_handler_pre(self):
   scn = bpy.context.scene
   global resultingObjects

   showOnAllLayers(scn)
   #bpy.ops.object.mode_set(mode='OBJECT')
   bpy.ops.object.select_all(action='DESELECT')

   logPrint1("gen_blob_animation",["pre"])
   logPrint1("gen_blob_animation",[resultingObjects])


   #for k,v in self.resultingObjects.items():
       #logPrint1("gen_blob_animation",[o[1]])

   #    v.select = True

   #bpy.ops.object.delete()

   resultingObjects= gen_blob_general()
   keepSingleLayer(scn,scn.put_on_layer)





#
#  Run render only from script! NOT GUI (contexts won't match)
#
def gen_blob_render_handler_post(self):
   scn = bpy.context.scene
   global resultingObjects

   showOnAllLayers(scn)
   bpy.ops.object.select_all(action='DESELECT')

   logPrint1("gen_blob_animation",["post"])
   logPrint1("gen_blob_animation",[resultingObjects])

   for k,v in resultingObjects.items():
       #logPrint1("gen_blob_animation",[o[1]])
       #showOnAllLayers(v)
       v.select = True

   bpy.ops.object.delete()
   keepSingleLayer(scn,scn.put_on_layer)












def render_ready_callback(self, context):
   render_ready_callback___();







def render_ready_callback___():
   scn = bpy.context.scene
   render_ready = scn.render_ready

   if render_ready :

      if not gen_blob_render_handler_pre.__name__ in [hand.__name__ for hand in bpy.app.handlers.render_pre]:
          bpy.app.handlers.render_pre.append(gen_blob_render_handler_pre)

      if not gen_blob_render_handler_post.__name__ in [hand.__name__ for hand in bpy.app.handlers.render_post]:
          bpy.app.handlers.render_post.append(gen_blob_render_handler_post)

   else :

      bpy.app.handlers.render_pre.remove( gen_blob_render_handler_pre )

      bpy.app.handlers.render_post.remove( gen_blob_render_handler_post)

















class Blobby(bpy.types.Panel):
    bl_label = "Blobby"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"



    def draw(self, context):

        layout = self.layout
        scn = context.scene
        layout.operator("blobby.gen_blob")
        layout.prop(scn, 'name_of_blob')
        layout.prop(scn, 'workgroup')
        layout.prop(scn, 'put_on_layer')
        layout.prop(scn, 'frame_start_')
        layout.prop(scn, 'frame_end_')
        layout.prop(scn, 'script')
        layout.prop(scn, 'escad')
        layout.prop(scn, 'tmpDir')
        layout.operator("blobby.gen_blob_anim")
        layout.prop(scn, 'render_ready')
        layout.prop(scn, 'use_script')
        #layout.prop(scn, 'use_openscad')
        layout.prop(scn, 'export_w_rotation_in_deg')
        layout.prop(scn, 'useCork')




def blobbyProperties():


   bpy.types.Scene.name_of_blob = StringProperty(
         name = "Name of blob"
        ,default = ""
        )

   bpy.types.Scene.workgroup = StringProperty(
         name = "Groups"
        ,default = ""
        )

   bpy.types.Scene.put_on_layer = IntProperty(
         name = "Put result on layer"
        ,default = 0
        )

   bpy.types.Scene.frame_start_ = IntProperty(
       name = "Frame start"
      ,default = 0
      )

   bpy.types.Scene.frame_end_ = IntProperty(
       name = "Frame end"
      ,default = 0
      )

   bpy.types.Scene.script = StringProperty(
         name = "Script file"
        ,default = ""
        )
   bpy.types.Scene.escad = StringProperty(
         name = "ESCAD file"
        ,default = ""
        )
   bpy.types.Scene.tmpDir = StringProperty(
         name = "Temporal directory"
        ,default = "/tmp"
        )

   bpy.types.Scene.render_ready = BoolProperty(
       name = "Render ready"
      ,description = "Set handlers for rendering"
      ,default = False
      ,update = render_ready_callback
      )

   bpy.types.Scene.use_script = BoolProperty(
       name = "Use script"
      ,description = "Run commands from script"
      ,default = False
      #,update = render_ready_callback
      )
      
   #bpy.types.Scene.use_openscad = BoolProperty(
   #    name = "Use openscad"
   #   ,description = "Instead of implicitcad use openscad"
   #   ,default = False
   #   #,update = render_ready_callback
   #   )
      
   bpy.types.Scene.export_w_rotation_in_deg = BoolProperty(
       name = "w in deg"
      ,description = "Export w_rotation in deg"
      ,default = False
      #,update = render_ready_callback
      )
   bpy.types.Scene.useCork = BoolProperty(
       name = "Use cork"
      ,description = "Use cork as backend for boolean operations"
      ,default = False
      #,update = render_ready_callback
      )


   return



def blobbyProperties_del():

   del bpy.types.Scene.name_of_blob

   del bpy.types.Scene.workgroup

   del bpy.types.Scene.put_on_layer

   del bpy.types.Scene.frame_start_

   del bpy.types.Scene.frame_end_

   del bpy.types.Scene.script
   
   del bpy.types.Scene.escad

   del bpy.types.Scene.tmpDir

   del bpy.types.Scene.render_ready

   del bpy.types.Scene.use_script
   
   #del bpy.types.Scene.use_openscad
   
   del bpy.types.Scene.export_w_rotation_in_deg
   
   del bpy.types.Scene.useCork

   return




def register():
   bpy.utils.register_module(__name__)
   blobbyProperties()

def unregister():
    bpy.utils.unregister_module(__name__)
    blobbyProperties_del()



if __name__ == "__main__":
    register()


class GEN_BLOB_OT_Blobby(bpy.types.Operator):
    bl_idname = "blobby.gen_blob"
    bl_label = "Single update"



    def execute(self, context):
        #scn=bpy.context.scene
        #system( "" + scn.tmpDir)

        gen_blob_general()

        return{'FINISHED'}




class GEN_BLOB_OT_Blobby_anim(bpy.types.Operator):
    bl_idname = "blobby.gen_blob_anim"
    bl_label = "Animation"

    resultingObjects={}

    def gen_blob_animation(self):
        scn = bpy.context.scene

        for frame in range(scn.frame_start_, scn.frame_end_ + 1):
            scn.frame_set(frame)
            showOnAllLayers(scn)
            #bpy.ops.object.mode_set(mode='OBJECT')
            bpy.ops.object.select_all(action='DESELECT')

            logPrint1("gen_blob_animation",[self.resultingObjects])


            #for k,v in self.resultingObjects.items():
                #logPrint1("gen_blob_animation",[o[1]])

            #    v.select = True

            #bpy.ops.object.delete()

            self.resultingObjects= gen_blob_general()
            keepSingleLayer(scn,scn.put_on_layer)


    def execute(self, context):
      self.gen_blob_animation()

      return{'FINISHED'}





def gen_blob_general():

    scn = bpy.context.scene
    script = scn.script
    use_script = scn.use_script

    if use_script:
        with io.open(script, 'r', encoding='utf-8') as f:
            x = BJSParser(f.read())

        t=x.parseBJS()
        logPrint1("gen_blob_general",[t])


        e = executeBJS()
        ro= e.executeBJS(t["return"])

        #del x
        #del e
        return(ro)

    else:
        keepSingleLayer(
                   joinGroup(scn.workgroup,scn.objects[scn.name_of_blob])
                   ,scn.put_on_layer
                  )


logKeys = [ "gen_blob_general"
           ,"gen_blob_animation"
           ,"addVerticesInsideSelectionVolumeToVertexGroup"
           ,"toMeshInPlace"
           ,"joinOneMore"
           ,"communicate_with_GenImplicit_via_json"
          ]



def logPrint(key,keys,p,params):
    if key in keys:
        p(*params)

def logPrint1(key,params):
    logPrint(key,logKeys,print,params)

###productivity section===============================
#=====================================================


def add_initial_properties(export_group):

    for obj in bpy.data.scenes[bpy.context.scene].objects:
        if not (export_group == ''):
            if (obj.name in bpy.data.groups[export_group].objects) :
                obj["rounding"] = 0.0
                obj["toOpenSCAD"] = 1
                obj["type"] = "Sphere"


def update_equal_values(value,value_updated,update_group):
    for obj in bpy.data.scenes[bpy.context.scene].objects:
        if not (update_group == ''):
            if (obj.name in bpy.data.groups[update_group].objects
                    and obj["rounding"] == value) :
                obj["rounding"] = value_updated

def selectBy_equal_rounding_values(value,select_group):
    for obj in bpy.data.scenes[bpy.context.scene.name].objects:
        if not (select_group == ''):
            if (obj.name in bpy.data.groups[select_group].objects
                    and obj["rounding"] == value) :
                obj.select = True
        else:
            if (obj["rounding"] == value) :
                obj.select = True

def setRounding(obj,r):
    obj["rounding"] = r

def update_selected(fUpdate, params):
    for obj in bpy.context.selected_objects:
        fUpdate(obj, params)

def duplicateList(lst):
    new_lst = []
    for obj in lst:
        new_lst.append(
                       duplicateObject(obj)
                      )

    return new_lst


def duplicateObject(obj):
    new_obj = obj.copy()
    new_obj.data = obj.data.copy()
    new_obj.animation_data_clear()
    bpy.context.scene.objects.link(new_obj)
    return new_obj



def applyModifier(baseObject,modname):
    scn=bpy.context.scene
    layers=list(scn.layers)
    showOnAllLayers(scn)
    bpy.ops.object.select_all(action='DESELECT')
    baseObject.select = True
    scn.objects.active = baseObject
    bpy.ops.object.modifier_apply(apply_as='DATA', modifier=modname)
    bpy.ops.object.select_all(action='DESELECT')
    scn.layers=layers


def toMeshInPlace(obj):
    # --- setup variables ---
    context = bpy.context
    scene = context.scene

    # assume we have an object with geometry, nurbs, metaball, curves... _not_ a lamp
    #obj = context.object
    logPrint1("toMeshInPlace",["toMeshInPlace " + obj.name])

    # --- get a mesh from the object ---
    apply_modifiers = True
    settings = 'RENDER'
    mesh = obj.to_mesh(scene, apply_modifiers, settings)
    obj.modifiers.clear()
    old_mesh=obj.data
    obj.data = mesh
    removeMeshFromMemory (old_mesh.name)
    logPrint1("toMeshInPlace","toMeshInPlace done")



def removeMeshFromMemory (passedName):
    logPrint1("toMeshInPlace",["removeMeshFromMemory:[%s]." % passedName])
    # Extra test because this can crash Blender if not done correctly.
    result = False
    mesh = bpy.data.meshes.get(passedName)
    if mesh != None:
        if mesh.users == 0:
            try:
                mesh.user_clear()
                can_continue = True
            except:
                can_continue = False

            if can_continue == True:
                try:
                    bpy.data.meshes.remove(mesh)
                    result = True
                    logPrint1("toMeshInPlace",["removeMeshFromMemory: MESH [" + passedName + "] removed from memory."])
                except:
                    result = False
                    logPrint1("toMeshInPlace",["removeMeshFromMemory: FAILED to remove [" + passedName + "] from memory."])
            else:
                # Unable to clear users, something is holding a reference to it.
                # Can't risk removing. Favor leaving it in memory instead of risking a crash.
                logPrint1("toMeshInPlace",["removeMeshFromMemory: Unable to clear users for MESH, something is holding a reference to it."])
                result = False
        else:
            logPrint1("toMeshInPlace",["removeMeshFromMemory: Unable to remove MESH because it still has [" + str(mesh.users) + "] users."])
    else:
        # We could not fetch it, it does not exist in memory, essentially removed.
        logPrint1("toMeshInPlace",["We could not fetch MESH [%s], it does not exist in memory, essentially removed." % passedName])
        result = True
    return result



def addBooleanModifier(baseObject,obj,applyMod=True):
    def prepareForExport(o):
        mid = str(len(o.modifiers))
        modname=(mid + "_Triangulate")
        o.modifiers.new(modname, type='TRIANGULATE')
        o.modifiers[modname].quad_method="BEAUTY"
        o.modifiers[modname].ngon_method="BEAUTY"
        return(modname)

    scn=bpy.context.scene

    if scn.useCork:

        layers=list(scn.layers)
        f1=scn.tmpDir + "/1.off"
        f2=scn.tmpDir + "/2.off"
        f3=scn.tmpDir + "/3.off"

        showOnAllLayers(scn)
        bpy.ops.object.select_all(action='DESELECT')

        baseObject.select = True
        scn.objects.active = baseObject

        mn=prepareForExport(baseObject)
        bpy.ops.export_mesh.off(filepath=f1)
        baseObject.modifiers.remove(baseObject.modifiers[mn])

        bpy.ops.object.select_all(action='DESELECT')
        obj.select = True
        scn.objects.active = obj

        mn=prepareForExport(obj)
        bpy.ops.export_mesh.off(filepath=f2)
        obj.modifiers.remove(obj.modifiers[mn])

        bpy.ops.object.select_all(action='DESELECT')

        system( "cork " + " -union " + f1 + " " + f2 + " " + f3)
        bpy.ops.import_mesh.off(filepath=f3)
        #bpy.ops.object.shade_smooth();

        old_mesh= baseObject.data
        baseObject.data = bpy.context.active_object.data

        scn.objects.active.select = True
        bpy.ops.object.delete()
        removeMeshFromMemory (old_mesh.name)



        system( "rm " + f1)
        system( "rm " + f2)
        system( "rm " + f3)
        scn.objects.active = baseObject
        bpy.ops.object.select_all(action='DESELECT')
        scn.layers=layers
    else:
        boolOpType = baseObject.get("boolOperation", 'UNION')
        mid = str(len(baseObject.modifiers))
        modname=(mid + "_" + boolOpType + "_to_" + obj.name)

        baseObject.modifiers\
                     .new(modname, type='BOOLEAN')

        baseObject.modifiers[modname].operation        = boolOpType
        baseObject.modifiers[modname].double_threshold =\
                                float(baseObject.get("bool_double_thrashold", 0.05))
        baseObject.modifiers[modname].solver           = \
                                            baseObject.get("bool_solver", "BMESH")
        baseObject.modifiers[modname].object           = obj

        if applyMod :
           applyModifier(baseObject,modname)
#end of addBooleanModifier-----------------------------


def addRemeshModifier(baseObject):
    mid = str(len(baseObject.modifiers))
    modname=mid + "_Remesh"
    baseObject.modifiers.new(modname,type='REMESH')
    baseObject.modifiers[modname].octree_depth =int(baseObject.get("remesh_octree_depth", 4))
    baseObject.modifiers[modname].scale = float(baseObject.get("remesh_scale", 0.990))

    baseObject.modifiers[modname].use_smooth_shade =\
                            bool(baseObject.get("remesh_use_smooth_shade", True))

    baseObject.modifiers[modname].mode =baseObject.get("remesh_mode", "SMOOTH")

    applyModifier(baseObject,modname)
#end of addRemeshModifier------------------------------


def addSmoothModifier(baseObject):
    mid = str(len(baseObject.modifiers))
    modname=mid + "_Smooth"
    baseObject.modifiers.new(modname,type='SMOOTH')
    baseObject.modifiers[modname].factor = float(baseObject.get("smooth_factor", 1))
    baseObject.modifiers[modname].iterations =int(baseObject.get("smooth_iterations", 1))
    baseObject.modifiers[modname].vertex_group =baseObject.get("smoothVertexGroup", "")

    applyModifier(baseObject,modname)
#end of addSmoothModifier-----------------------------

def addSubSurfModifier(baseObject):
    mid = str(len(baseObject.modifiers))
    modname=mid + "_Subdivision Surface"
    baseObject.modifiers.new(modname,type='SUBSURF')
    baseObject.modifiers[modname].levels =int(baseObject.get("subSurf_levels", 2))

    applyModifier(baseObject,modname)
#end of addSmoothModifier-----------------------------



def joinGroup(group,baseObject,applyMod=True):

    def joinOneMore(baseObject,obj):
        if not (obj == baseObject):
            logPrint1("joinOneMore",[baseObject.name,obj.name])
            addBooleanModifier(baseObject,obj,applyMod)
    #end of joinOneMore------------------------------------

    if not (group == ''):
        result = duplicateObject(baseObject)
        toMeshInPlace(result)
        without_baseObject = []
        for o in bpy.data.groups[group].objects:
            if     (not o == baseObject)\
               or (not o == result):
                without_baseObject.append(o)

        for obj in without_baseObject:
            joinOneMore(result,obj)

        #addRemeshModifier(result)
        #addSmoothModifier(result)
        #addSubSurfModifier(result)



        if bool(result.get("addRemeshModifier",False))==True:
            addRemeshModifier(result)
        if bool(result.get("addSmoothModifier",False))==True:
            addSmoothModifier(result)
        if bool(result.get("addSubSurfModifier",False))==True:
            addSubSurfModifier(result)

        return result








#bpy.ops.object.select_all(action=‘DESELECT’)

def keepSingleLayer(obj,l):
    t=int(l)
    obj.layers[t] = True
    for i in range(len(obj.layers)):
        obj.layers[i] = (i==t)


def showOnAllLayers(obj):
    for i in range(len(obj.layers)):
        obj.layers[i] = True



def communicate_with_GenImplicit_via_json(json_file_name, export_group, Scene):

    j = []
    dummy = bpy.data.objects.new('dummy_lrs', None)
    for obj in bpy.data.scenes[Scene].objects:
        if not (export_group == ''):
            if (obj.name in bpy.data.groups[export_group].objects) and ("toOpenSCAD" in obj) :

                dummy.matrix_world = obj.matrix_world
                dummy.rotation_mode = 'AXIS_ANGLE'
                w_rotation, x_rotation, y_rotation, z_rotation  = dummy.rotation_axis_angle
                x_loc, y_loc, z_loc = dummy.matrix_world.to_translation()
                dummy.rotation_mode = 'XYZ'

                if bpy.context.scene.export_w_rotation_in_deg: 
                  w_rotation = degrees(w_rotation) 
                  logPrint1("communicate_with_GenImplicit_via_json",["export_w_rotation_in_deg"])


                #x_rotation, y_rotation, z_rotation  = obj.rotation_euler
                #w_rotation, x_rotation, y_rotation, z_rotation  = obj.rotation_axis_angle
                j.append ({ 'name' : obj.name ,
                            'type_': obj["type"] ,
                            'group': list(map(lambda g: g.name, list(obj.users_group))),
                            'x'    : x_loc ,
                            'y'    : y_loc ,
                            'z'    : z_loc ,
                            'dim_x'    : obj.dimensions.x ,
                            'dim_y'    : obj.dimensions.y ,
                            'dim_z'    : obj.dimensions.z ,
                            'scale_x'  : obj.scale.x ,
                            'scale_y'  : obj.scale.y ,
                            'scale_z'  : obj.scale.z ,
                            'rot_x'  : x_rotation ,
                            'rot_y'  : y_rotation ,
                            'rot_z'  : z_rotation ,
                            'rot_w'  : w_rotation
                         })
                if "rounding" in obj:
                   j[-1].update({'rounding' : obj["rounding"]})
                else:
                   j[-1].update({'rounding' : 0.0})
                #print(j)


    bpy.data.objects.remove(dummy,True)
    data = {
             'objects' : j
            ,'groups'  : list(map(lambda g: g.name, bpy.data.groups)),
           }


    #'/home/hokum/Documents/haskell/blender_haskell/gen_mesh/data.json'
    with io.open(json_file_name, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False)



def gen_implicit_general(group,baseObject):
  return gen_call_general(group,baseObject,"genmesh.sh")

def gen_scad_general(group,baseObject):
  return gen_call_general(group,baseObject,"genmesh_.sh") 
  
def gen_call_general(group,baseObject,execCmd):
    def dirAndFile(dir,file):
        if dir[-1]=="/":
            return(dir + file)
        else:
            return(dir + "/" + file)

    def copyCustomProps(s,t):
        for k,v in s.items():
            if k not in '_RNA_UI':
                t[k]=v

    scn = bpy.context.scene

    json_file_name_base = "toGenimplicit"
    json_file_dir       = scn.tmpDir

    stl_file_name_base = "fromGenimplicit"
    stl_file_dir       = scn.tmpDir

    mesh_quality       = baseObject.get("mesh_quality_full", 0.5)

    overall_rounding   = baseObject.get("overall_rounding", 0)
    escad = baseObject.get("escad", scn.escad)
    

    communicate_with_GenImplicit_via_json(
            dirAndFile(json_file_dir, json_file_name_base + "." + group)
            , group
            , scn.name
        );

    json_import_file = dirAndFile(json_file_dir, json_file_name_base + "." + group)
    stl_export_file_name = stl_file_name_base  + "." + group + ".stl"
    stl_export_file  = dirAndFile(stl_file_dir,  stl_export_file_name)
  
    command = execCmd + " " + json_import_file\
                  + " " + escad\
                  + " " + escad + "_"\
                  + " "     + str(mesh_quality)\
                  + " "  + stl_export_file\
                  
                  
                  

    logPrint1("gen_blob_general",[command])



    system(command);


    bpy.ops.import_mesh.stl(filepath=stl_export_file
                         # , filter_glob="*.stl"
                          , files=[{"name": stl_export_file_name}]
                          , directory=stl_file_dir);
    bpy.ops.object.shade_smooth();

    result = scn.objects[bpy.context.active_object.name]

    copyCustomProps(baseObject,result)


    if bool(result.get("addRemeshModifier",False))==True:
        addRemeshModifier(result)
    if bool(result.get("addSmoothModifier",False))==True:
        addSmoothModifier(result)
    if bool(result.get("addSubSurfModifier",False))==True:
        addSubSurfModifier(result)

    return(result)


###paser =================================
###=======================================

class Parsecish:
    s = ""
    nlsetting = "unix"

    def __init__(self, text):
        self.s = text

    def __del__(self):
        #del self.s
        #class_name = self.__class__.__name__
        #print class_name, "destroyed"
        return ()

    def stringToken(self,t):
        # 1 2 3 4 5 len
        # 0 1 2 3 4 ind
        # a b c d e str
        # . . . - - s[:3]
        # - - - . . s[3:]
        left = self.s[:len(t)]
        right = self.s[len(t):]
        if t==left :
            self.s = right
            logPrint1("stringToken",["stringToken t=" + t + " left=" + left + " right=" + right])
            return {"result": "good", "return": t}
        else:
            self.s = right
            logPrint1("stringToken",["stringToken t=" + t + " left=" + left + " right=" + right])
            return {"result": "fail", "return": ""}


    def tryToken(self,token):
        sb = self.s
        r = token[0](*token[1])
        if r["result"] == "fail" :
            self.s = sb
            return {"result": "bad", "return": ""}
        else:
            return {"result": "good", "return": r}

    def tokenIsGood(self,r):
        return r["result"] == "good"

    def tokenIsBad(self,r):
        return r["result"] == "bad"

    def tokenFailed(self,r):
        return r["result"] == "fail"


    def many(self,token):
        def appendIfNotEmpty(x,y):
            if (not y=="")and(not y==[]):
                x.append(y)

        ret = []
        while True:
            sp = self.s
            r = token[0](*token[1])
            appendIfNotEmpty(ret,r["return"])
            logPrint1("many",[self.s])
            if not self.tokenIsGood(r):
                self.s = sp
                break

        return {"result": "good", "return": ret}


    def anyChar(self):
        if not self.s == "" :
            ret = self.s[:1]
            self.s= self.s[1:]
            return {"result": "good", "return": ret}
        else:
            return {"result": "fail", "return": ""}


    def notFollowedBy(self,token):
        r = token[0](*token[1])
        if self.tokenIsGood(r):
            return {"result": "fail", "return": r["return"]}
        else:
            return {"result": "good", "return": r["return"]}


    def manyTill(self,token,till):
        def appendIfNotEmptyOrMustEnd(x,y,t):
            if (not y=="") and (not self.tokenIsGood(t)):
                x.append(y)

        ret = []
        while True:
            sp = self.s
            t = self.tryToken(till)
            r = token[0](*token[1])
            appendIfNotEmptyOrMustEnd(ret,r["return"],t)
            logPrint1("manyTill",[self.s])
            if self.tokenIsGood(t):
                self.s = sp
                return {"result": "good", "return": ret}
                break
            elif (not self.tokenIsGood(t)) and (not self.tokenIsGood(r)):
                self.s = sp
                return {"result": "fail", "return": ""}
                break





    def choice(self,tokens):
        sp= self.s
        r={"result": "fail", "return": []}
        for token in tokens:
            logPrint1("choice",[token])
            r = token[0](*token[1])
            if not self.tokenFailed(r):
                break
            else:
                self.s = sp
        return (r)


    def space(self):
        return (self.choice([
                            (self.stringToken,[" "]) ##space
                          , (self.stringToken,["\t"]) ##tab
                          , (self.stringToken,["\n"])
                          , (self.stringToken,["\r"])
                            ])
                )

    def eof(self):
        if len(self.s)>0:
            return {"result": "fail", "return": ""}
        else:
            return {"result": "good", "return": ""}


    def newline(self):
        if self.nlsetting== "unix":
            return (self.choice([
                                (self.stringToken,["\n"])
                                ])
                    )
        elif self.nlsetting== "windows":
            return (self.choice([
                                (self.stringToken,["\n"])
                               ,(self.stringToken,["\r"])
                                ])
                    )


    def spaces(self):

        return (self.many([self.space,[]]))


class BJSParser(Parsecish):
    def readName(self):
        return(#-------
               self.manyTill([self.anyChar,[]]
                            ,[self.space  ,[]]
                            )
              )#-------

    def skipTillNewLine(self):
        def thereIsNewLine():
            return(#-------
                   self.manyTill([self.space  ,[]]
                                ,[self.newline,[]]
                                )
                  )#-------

        return(#-------
               self.choice([
                           (thereIsNewLine,[])
                          ,(self.spaces,[])
                           ])
              )#-------

    def readTillNewLine(self):
        def thereIsNewLine():
            return(#-------
                   self.manyTill([self.anyChar,[]]
                                ,[self.newline,[]]
                                )
                  )#-------

        def thereIsNoNewLine():
            return(#-------
                   self.many([self.anyChar,[]])
                  )#-------

        return(#-------
               self.choice([
                           (thereIsNewLine  ,[])
                          ,(thereIsNoNewLine,[])
                           ])
              )#-------

    def allSucceeded(self,l):
        b=True
        for x in l:
            if not x["result"]=="good":
                b=False
        return(b)

    def allNotFailed(self,l):
        b=True
        for x in l:
            if x["result"]=="fail":
                b=False
        return(b)


    def attach(self):


        r= [
             self.spaces()                  #0
            ,self.stringToken("attach")     #1
            ,self.spaces()                  #2
            ,self.readName()                #3
            ,self.spaces()                  #4
            ,self.readName()                #5
            ,self.spaces()                  #6
            ,self.readName()                #7
            ,self.skipTillNewLine()         #8
            ]


        if self.allSucceeded(r):
            return {#---------------------------------------
                    "result": "good", "return":         [r[1]["return"]
                                                ,"".join(r[3]["return"])
                                                ,"".join(r[5]["return"])
                                                ,"".join(r[7]["return"])
                                                ]
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}


    def identiationSpace(self):
        return (self.choice([
                            (self.stringToken,[" "]) ##space
                          , (self.stringToken,["\t"]) ##tab
                            ])
                )

    def identiationSpaces(self):
        return (self.many([self.identiationSpace,[]]))

    def identiationLength(self,s):
        t=str(s[::-1])
        #logPrint1("identiationLength")
        #logPrint1(s)
        #logPrint1(t)
        x=BJSParser(t)
        #logPrint1(x.s)
        r=x.identiationSpaces()
        #logPrint1("identiationLength=",len(r["return"]),r)
        return(len(r["return"]))

    def joinLike(self,commandString):

        r= [
             self.spaces()                  #0
            ,self.stringToken(commandString) #1
            ,self.spaces()                  #2
            ,self.readName()                #3
            ,self.skipTillNewLine()         #4
            ,self.newline()                 #5
            ,self.readTillNewLine()         #6
            ,self.newline()                 #7
            ,self.readTillNewLine()         #8
            ]

        if self.allSucceeded(r):
            i=self.identiationLength("".join(r[0]["return"]))#len(r[0]["return"])
            #logPrint1(r[0]["return"])
            #logPrint1(i)
            return {#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                                               r[1]["return"]
                        ,                              "".join(r[3]["return"])
                        ,self.acknowledgeIdentiation(i,"".join(r[6]["return"]))
                        ,self.acknowledgeIdentiation(i,"".join(r[8]["return"]))
                        ]#//////////////////////////////////////////
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}


    def joinStrict(self):
        return(self.joinLike("joinStrict"))

    def join(self):
        return(self.joinLike("join"))

    def imp(self):
        return(self.joinLike("imp"))

    def scad(self):
        return(self.joinLike("scad"))

    def keepSingleLayer(self):

        r= [
             self.spaces()                           #0
            ,self.stringToken("keepSingleLayer")       #1
            ,self.spaces()                           #2
            ,self.readName()                          #3
            ,self.spaces()                             #4
            ,self.readName()                          #5
            ]

        if self.allSucceeded(r):
            return {#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[1]["return"]
                        ,"".join(r[3]["return"])
                        ,"".join(r[5]["return"])
                        ]#//////////////////////////////////////////
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}



    def acknowledgeIdentiation(self,i,s):
        if i>0:
            return(s[i:])
        else:
            return(s)

    def setting(self):
        r = [
              self.spaces()                    #0
             ,self.readName()                  #1
             ,self.skipTillNewLine()           #2
             ,self.newline()                   #3
             ,self.readTillNewLine()           #4
             ,self.tryToken([self.newline,[]]) #5
            ]
        #logPrint1(r)
        if self.allNotFailed(r):
            return{#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[1]["return"]
                        ,"".join(r[4]["return"])
                        ]#//////////////////////////////////////////
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}


    def manySettings(self):
        return(self.many([self.setting,[]]) )




    def set(self):

        def processSetting(i,s):
            s[0]=                              "".join(s[0])
            s[1]=self.acknowledgeIdentiation(i,"".join(s[1]))
            return (s)

        def processSettings(i,l):
            #logPrint1(l)
            ret=[]
            for s in l:
                ret.append(processSetting(i,s))
            return(ret)

        r= [
             self.spaces()                    #0
            ,self.stringToken("set")          #1
            ,self.spaces()                    #2
            ,self.readName()                  #3
            ,self.setting()                   #4
            ,self.manySettings()              #5
            ]
        #logPrint1(r)
        if self.allSucceeded(r):
            i=self.identiationLength("".join(r[0]["return"]))#len(r[0]["return"])
            #logPrint1(r[0]["return"])
            #logPrint1(i)
            return {#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[1]["return"]
                        ,"".join(r[3]["return"])
                        ]#//////////////////////////////////////////
                        + [processSetting (i,r[4]["return"])]
                        + processSettings(i,r[5]["return"])
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}

    def filterEmptyReturns(self,l):
        l["return"]=self.filterEmptyInList(l["return"])
        return(l)


    def filterEmptyInList(self,l):
        ret=[]
        for x in l:
            if len(x)>0:
                ret.append(x)
        l=ret
        return(l)






    def selector(self):
        r = [
              #self.newline()                   #0
             self.readTillNewLine()           #0
             ,self.tryToken([self.newline,[]]) #1
            ]
        #logPrint1(r)
        if self.allNotFailed(r):
            return{#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[0]["return"]
                        ]#//////////////////////////////////////////
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}


    def manySelectors(self):
        def thereIsDoubleNewLine():
            r = [
                  self.newline()                   #0
                 ,self.newline()                   #1
                ]
            #logPrint1(r)
            if self.allNotFailed(r):
                return{#---------------------------------------
                        "result": "good", "return": [#//////////////////
                                     r[0]["return"]
                                    ,r[1]["return"]
                            ]#//////////////////////////////////////////
                       }#----------------------------------------
            else:
                return {"result": "fail", "return": []}


        def nlnlOrEOF():
            return(
                    self.choice([
                                   (thereIsDoubleNewLine,[])
                                  ,(self.eof,[])
                                ])
                  )




        return(self.manyTill([self.selector,[]]
                            ,[ nlnlOrEOF   ,[]]
                            )
              )



    def smoothArea(self):

        def processSelector(i,s):
            s[0]=self.acknowledgeIdentiation(i,"".join(s[0]))
            return (s)

        def processSelectors(i,l):
            #logPrint1(l)
            ret=[]
            for s in l:
                ret.append(processSelector(i,s))
            return(ret)

        r= [
             self.spaces()                    #0
            ,self.stringToken("smoothArea")   #1
            ,self.spaces()                    #2
            ,self.readName()                  #3
            ,self.skipTillNewLine()           #4
            ,self.newline()                   #5
            #,self.selector()                  #6
            ,self.manySelectors()             #6
            ]
        #logPrint1(r)
        if self.allSucceeded(r):
            i=self.identiationLength("".join(r[0]["return"]))#len(r[0]["return"])
            #logPrint1(r[0]["return"])
            #logPrint1(i)
            return {#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[1]["return"]
                        ,"".join(r[3]["return"])
                        ]#//////////////////////////////////////////
                    #    + [processSetting (i,r[6]["return"])]
                        + processSelectors(i,r[6]["return"])
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}



    def comment(self):

        r = [
              self.spaces()                   #0
             ,self.stringToken("--")                   #1
             ,self.readTillNewLine()           #2
             ,self.tryToken([self.newline,[]]) #3
            ]
            #logPrint1(r)
        if self.allNotFailed(r):
            return{#---------------------------------------
                    "result": "good", "return": [#//////////////////
                                 r[2]["return"]
                        ]#//////////////////////////////////////////
                   }#----------------------------------------
        else:
            return {"result": "fail", "return": []}


    def irrelevant(self):
        return(
                self.choice([
                               (self.comment,[])
                              ,(self.spaces,[])
                            ])
              )


    def skipIrrelevantTill(self,till):
        return(self.manyTill([self.irrelevant,[]]
                            ,till
                            )
              )



    def parseBJS(self): #parse Blender Join Script
        def possibleParsers():
            return(#-------
                   self.choice([
                               (self.join  ,[])
                              ,(self.imp   ,[])
                              ,(self.scad  ,[])
                              ,(self.attach,[])
                              ,(self.set   ,[])
                              ,(self.keepSingleLayer,[])
                              ,(self.smoothArea,[])
                              ,(self.comment,[])
                               ])
                  )#-------


        return(#------------------------------
                self.filterEmptyReturns(#...........
                            self.many([#,,,,,,,,,
                                        possibleParsers
                                       ,[]
                                      ])#,,,,,,,,
                            )#..........
              )#-------------------------------

        #return(self.many([possibleParsers,[]]))

###=======================================
###end of parser =========================


class executeBJS:
    #bjs=[]
    resultingObjects={}

    #def __init__(self):
        #self.bjs = script
        #return ()

    def __del__(self):
        #del self.s
        #class_name = self.__class__.__name__
        #logPrint1 class_name, "destroyed"
        return ()


    def executeJoin(self,command):
        commandIsJoin            = command[0]=="join"
        numberOfArgumentsMatches = len(command)==4
        if numberOfArgumentsMatches:
            nameIsSane               = len(command[1])>0
            baseObjectNameIsSane     = len(command[2])>0
            baseGroupNameIsSane      = len(command[3])>0

            if      commandIsJoin            \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and baseObjectNameIsSane     \
                and baseGroupNameIsSane      :

                self.resultingObjects[command[1]]=joinGroup(command[3]
                                                           ,bpy.context.scene.objects[command[2]]
                                                           ,False
                                                           )
                #return(self.resultingObjects[command[1]])

    def executeJoinStrict(self,command):
        commandIsJoin            = command[0]=="joinStrict"
        numberOfArgumentsMatches = len(command)==4
        if numberOfArgumentsMatches:
            nameIsSane               = len(command[1])>0
            baseObjectNameIsSane     = len(command[2])>0
            baseGroupNameIsSane      = len(command[3])>0

            if      commandIsJoin            \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and baseObjectNameIsSane     \
                and baseGroupNameIsSane      :

                self.resultingObjects[command[1]]=joinGroup(command[3]
                                                           ,bpy.context.scene.objects[command[2]]
                                                           )
                #return(self.resultingObjects[command[1]])


    def executeImp(self,command):
        commandIsImp            = command[0]=="imp"
        numberOfArgumentsMatches = len(command)==4
        if numberOfArgumentsMatches:
            nameIsSane               = len(command[1])>0
            baseObjectNameIsSane     = len(command[2])>0
            baseGroupNameIsSane      = len(command[3])>0

            if      commandIsImp            \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and baseObjectNameIsSane     \
                and baseGroupNameIsSane      :

                self.resultingObjects[command[1]]=gen_implicit_general(command[3]
                                                           ,bpy.context.scene.objects[command[2]]
                                                           )
                #return(self.resultingObjects[command[1]])

    def executeScad(self,command):
        commandIsScad            = command[0]=="scad"
        numberOfArgumentsMatches = len(command)==4
        if numberOfArgumentsMatches:
            nameIsSane               = len(command[1])>0
            baseObjectNameIsSane     = len(command[2])>0
            baseGroupNameIsSane      = len(command[3])>0

            if      commandIsScad            \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and baseObjectNameIsSane     \
                and baseGroupNameIsSane      :

                self.resultingObjects[command[1]]=gen_scad_general(command[3]
                                                           ,bpy.context.scene.objects[command[2]]
                                                           )
                #return(self.resultingObjects[command[1]])



    def executeAttach(self,command):
        commandIsAttach            = command[0]=="attach"
        numberOfArgumentsMatches   = len(command)==4
        if numberOfArgumentsMatches:
            nameIsSane                 = len(command[1])>0
            firstObjectNameIsSane      = len(command[2])>0
            secondObjectNameIsSane     = len(command[3])>0

            if      commandIsAttach          \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and firstObjectNameIsSane    \
                and secondObjectNameIsSane   :

                r1=self.resultingObjects[command[2]]
                r2=self.resultingObjects[command[3]]

                addBooleanModifier(r1,r2)
                if bool(r1.get("addRemeshModifier",False))==True:
                    addRemeshModifier(r1)
                if bool(r1.get("addSmoothModifier",False))==True:
                    addSmoothModifier(r1)
                if bool(r1.get("addSubSurfModifier",False))==True:
                    addSubSurfModifier(r1)

                self.resultingObjects[command[1]]=r1
                #keepSingleLayer(r1,layer1)
                #keepSingleLayer(r2,layer2)


    def executeSet(self,command):
        def setProperty(name,value):
            self.resultingObjects[command[1]][name]=value

        commandIsSet            = command[0]=="set"
        numberOfArgumentsMatches   = len(command)>=3
        if numberOfArgumentsMatches:
            nameIsSane                 = len(command[1])>0
            propertyNameIsSane         = len(command[2][0])>0
            #valueNameIsSane            = len(command[2][1])>0

            if      commandIsSet          \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and propertyNameIsSane    :
                #and secondObjectNameIsSane   :

                for c in command[2:] :
                    setProperty(c[0],c[1])

    def executeKeepSingleLayer(self,command):

        commandIsKeepSingleLayer   = command[0]=="keepSingleLayer"
        numberOfArgumentsMatches   = len(command)==3
        if numberOfArgumentsMatches:
            nameIsSane                 = len(command[1])>0
            layerIsSane         = len(command[2])>0
            #valueNameIsSane            = len(command[2][1])>0

            if      commandIsKeepSingleLayer          \
                and numberOfArgumentsMatches \
                and nameIsSane               \
                and layerIsSane    :

                keepSingleLayer(self.resultingObjects[command[1]],command[2])



    def executeSmoothArea(self,command):
        def setProperty(name,value):
            self.resultingObjects[command[1]][name]=value

        logPrint1("addVerticesInsideSelectionVolumeToVertexGroup",["smoothArea"])

        commandIsSmoothArea        = command[0]=="smoothArea"
        numberOfArgumentsMatches   = len(command)>=3
        if numberOfArgumentsMatches:
            nameIsSane                 = len(command[1])>0
            #propertyNameIsSane         = len(command[2][0])>0
            #valueNameIsSane            = len(command[2][1])>0

            if      commandIsSmoothArea          \
                and numberOfArgumentsMatches \
                and nameIsSane:


                obj=self.resultingObjects[command[1]]

                selectors=[]
                for s in command[2:] :
                    selectors.append(bpy.context.scene.objects[s[0]])

                i=1
                while (True):
                    if ("smoothArea" + str(i)) in obj.vertex_groups:
                        i=i+1
                    else:
                        vertexGroup=("smoothArea" + str(i))
                        break


                addVerticesInsideSelectionVolumeToVertexGroup(obj,selectors,vertexGroup)

                setProperty("smoothVertexGroup",vertexGroup)

                addSmoothModifier(obj)

                del obj["smoothVertexGroup"]
                obj.vertex_groups.remove(obj.vertex_groups[vertexGroup])




    def executeBJS(self,bjs):
        for command in bjs:
            self.executeJoin(command)
            self.executeScad(command)
            self.executeImp(command)
            self.executeAttach(command)
            self.executeSet(command)
            self.executeKeepSingleLayer(command)
            self.executeSmoothArea(command)
        return(self.resultingObjects)







def isInsideNotConcave(point,obj):
    polygons = obj.data.polygons
    isInside = True
    for p in polygons:
        d=mathutils.geometry.distance_point_to_plane(point \
                                            ,obj.matrix_world * p.center \
                                            ,obj.matrix_world.to_3x3() * p.normal)
        if d>0:
            isInside=False

    return(isInside)



def deselectVertices(obj):

    for v in obj.data.vertices:
        v.select=False

    for e in obj.data.edges:
        e.select=False

    #for l in obj.data.loops:
    #    l.select=False

    for p in obj.data.polygons:
        p.select=False




def addVerticesInsideSelectionVolumeToVertexGroup(obj,selectors,vertexGroup):

    def selectVerticesInsideSelector(vertices,verts,selector):
        for v in vertices:
            if isInsideNotConcave(obj.matrix_world * v.co, selector):
                v.select=bool(selector.get("isAddingSelector",True))
                verts.append(v.index)

    vertices = obj.data.vertices
    deselectVertices(obj)

    verts=[]


    for selector in selectors:
        selectVerticesInsideSelector(vertices,verts,selector)



    if vertexGroup in obj.vertex_groups:
        logPrint1("addVerticesInsideSelectionVolumeToVertexGroup",[vertexGroup + " exists"])

        obj.vertex_groups.remove(obj.vertex_groups[vertexGroup])
        vg=obj.vertex_groups.new(name=vertexGroup)
        vg.add(verts, 1.0, 'ADD')
    else:
        logPrint1("addVerticesInsideSelectionVolumeToVertexGroup",[vertexGroup + " new"])
        vg=obj.vertex_groups.new(name=vertexGroup)
        vg.add(verts, 1.0, 'ADD')




##===========================================================
###end of productivity section===============================
