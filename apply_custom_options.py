import bpy
import io
import mathutils
#from mathutils import Matrix
from math import radians
from blobby import *


def add_initial_properties(export_group):

    for obj in bpy.context.scene.objects:
        if not (export_group == ''):
            if (obj.name in bpy.data.groups[export_group].objects) :
                obj["rounding"] = 0.0
                obj["toOpenSCAD"] = 1
                obj["type"] = "Sphere"


def update_equal_values(value,value_updated,update_group):
    for obj in bpy.data.scenes[bpy.context.scene].objects:
        if not (update_group == ''):
            if (obj.name in bpy.data.groups[update_group].objects
                    and obj["rounding"] == value) :
                obj["rounding"] = value_updated

def selectBy_equal_rounding_values(value,select_group):
    for obj in bpy.data.scenes[bpy.context.scene.name].objects:
        if not (select_group == ''):
            if (obj.name in bpy.data.groups[select_group].objects
                    and obj["rounding"] == value) :
                obj.select = True
        else:
            if (obj["rounding"] == value) :
                obj.select = True

def setRounding(obj,r):
    obj["rounding"] = r

def update_selected(fUpdate, params):
    for obj in bpy.context.selected_objects:
        fUpdate(obj, params)




#selectBy_equal_rounding_values(10,"fullBody")
#update_selected(setRounding, 10.0)
#joinSelected_Base(3,0.990,0.1,10,1)
#joinGroup("g1",bpy.context.active_object)
#r1=joinGroup("g1",bpy.context.scene.objects["baseg1"])
#r2=joinGroup("g2",bpy.context.scene.objects["baseg2"])
#addBooleanModifier(r1,r2)
#addRemeshModifier(r1)
#addSmoothModifier(r1)
#addSubSurfModifier(r1)
#keepSingleLayer(r1,10)
#keepSingleLayer(r2,2)

#x = Parsecish("   abcabcabcdefabc")
#x = BJSParser("       attach      aaaa  bbb     \n   cccccc    \n    xxx")
#x = BJSParser("join      aaaa \nbbb\ncccccc\n    xxx")
#def abcT():
#    x.stringToken("abc")

print("--------")
#print(x.many(abcT))
print("--------")
#print(x.spaces() )
#print(x.manyTill([x.stringToken,["abc"]],[x.stringToken,["def"]]))
#print(x.manyTill([x.anyChar,[]],[x.stringToken,["def"]]))
#print(x.many([x.stringToken,["abc"]]))
#print( x.choice([(x.stringToken,["xxx"]), (x.stringToken,["abc"]), (x.stringToken,["def"])]) )

#print(x.attach())
#print(x.join())

#with io.open("/home/user/.gvfs/1/blender_scripts/1.bjs", 'r', encoding='utf-8') as f:
#    x = BJSParser(f.read())

#t=x.parseBJS()
#print(t)

#e = executeBJS()
#e.executeBJS(t["return"])

#del x
#del e
#grp = bpy.data.groups.get('bacon')

#obj = bpy.data.objects['Cube']
#grp.objects.link(obj)



def makeOppositeSideObj(o,group,mirrorObject):
  no=duplicateObject(o)
  no.name=o.name + ".opposite"
  if (group + ".opposite") in bpy.data.groups:
      bpy.data.groups[group + ".opposite"].objects.link(no)
      #for g in o.users_group:
      #    if not g==group:
      #        bpy.data.groups[g].objects.link(no)
  else:
      bpy.ops.group.create(name=group + ".opposite")
      bpy.data.groups[group + ".opposite"].objects.link(no)

  l, r, sc = no.matrix_world.decompose()
  #rot = Matrix.Rotation(angle, size, axis)
  #rot = Matrix.reflect(rot)
  s=no.scale.copy()
  #nm.invert_safe()

  #e=no.matrix_world.to_euler('XYZ')
  #nm=mathutils.Matrix.identity(4)
  li,ri,si = mirrorObject.matrix_world.decompose()

  il = l.copy()
  ili= li.copy()
  il.resize_2d()
  ili.resize_2d()

  mo = mirrorObject.matrix_world.copy()
  print("mo",mo)
  mo *= mathutils.Matrix.Translation([1,0,0])
  print("mo",mo)
  ili1,_,_ = mo.decompose()
  ili1.resize_2d()
  (isectO,isectM) = mathutils.geometry.intersect_line_line([0,0],il,ili,ili1)
  print("isectOM",isectO,isectM)
  rd=r.rotation_difference(ri)
  #ld=l.reflect(l)

  #lip = li.copy()
  lip = mathutils.Vector((1.0, 0.0, 0.0))
  lip.rotate(ri)
  lip.resize_2d()


  lp = l.copy()
  lp.resize_2d()

  a_lp_lip = lip.angle_signed(lp,0)

  print( lip.angle_signed(lp,0) , lp,lip )

  isectM.resize_3d()
  ld=l-isectM
  ld.rotate(mathutils.Quaternion((0.0, 0.0, 1.0), 2*a_lp_lip))
  ld = ld+isectM
  #ld.rotate(mathutils.Quaternion((0.0, 0.0, 1.0), a_lp_lip))

  #ld.rotate(rd)
  #ld.negate()
  #rd.x = -rd.x
  #rd.z = -rd.z
  rd.y = -rd.y
  #ld.y  = -ld.y
  print(l,r)
  rm=rd.to_matrix().to_4x4()
  print(ld,rd)
  #s=o.scale.copy()
  #no.matrix_world = l * rm * s
  #no.rotation_quaternion = rd


  no.matrix_world = mirrorObject.matrix_world.copy()
  #no.matrix_world *= mathutils.Matrix.Translation(ld) * rm
  no.matrix_world *= rm
  no.location = ld
  #no.matrix_world *= mathutils.Matrix.Rotation(radians(180), 4, 'Z')
  no.scale=s
  #no.matrix_world *= mathutils.Matrix.Rotation(radians(180), 4, 'X')
  #no.scale=s
  #o.delta_rotation_euler = (0, 0, radians(20))




def makeOppositeSide(group,mirrorObject):
    if group in bpy.data.groups:
        for o in bpy.data.groups[group].objects:
            makeOppositeSideObj(o,group,mirrorObject)


def deleteOpposite(group):
    bpy.ops.object.select_all(action='DESELECT')
    if (group  + ".opposite") in bpy.data.groups:
        for o in bpy.data.groups[group + ".opposite"].objects:
            o.select = True

    bpy.ops.object.delete()


def addToGroup(group,obj):
    if group in bpy.data.groups:
        if not obj in list(bpy.data.groups[group].objects):
            bpy.data.groups[group].objects.link(obj)
    else:
        bpy.ops.group.create(name=group)
        if not obj in list(bpy.data.groups[group].objects):
            bpy.data.groups[group].objects.link(obj)


def addGroupToGroup(groupFrom,groupTo):
    if groupFrom in bpy.data.groups:
        for o in bpy.data.groups[groupFrom].objects:
            addToGroup(groupTo,o)

#deleteOpposite("group")
#deleteOpposite("sharpHull")
#deleteOpposite("smoothHull")
#deleteOpposite("sharpHullLeft")
#deleteOpposite("smoothHullLeft")

#makeOppositeSide("sharpHullLeft",bpy.context.scene.objects["mirrorGuide"])
#makeOppositeSide("smoothHullLeft",bpy.context.scene.objects["mirrorGuide"])

#addGroupToGroup("sharpHullLeft.opposite","sharpHull")
#addGroupToGroup("smoothHullLeft.opposite","smoothHull")
#addGroupToGroup("sharpHullLeft","sharpHull")
#addGroupToGroup("smoothHullLeft","smoothHull")

#makeOppositeSide("group",bpy.context.scene.objects["mirrorGuide"])
#makeOppositeSide("group","",0.5,4,'Y')
#makeOppositeSide("group","",0.5,4,'Z')





def is_inside(p, max_dist, obj):
    # max_dist = 1.84467e+19
    _, point, normal, face = obj.closest_point_on_mesh(p, max_dist)
    p2 = point-p
    v = p2.dot(normal)
    print(v)
    return not(v < 0.0)



def is_inside1(ray_origin, ray_destination, obj):

    # the matrix multiplations and inversions are only needed if you
    # have unapplied transforms, else they could be dropped. but it's handy
    # to have the algorithm take them into account, for generality.
    mat = obj.matrix_local.inverted()
    f = obj.ray_cast(mat * ray_origin, mat * ray_destination)
    _, loc, normal, face_idx = f

    if face_idx == -1:
        return False

    max_expected_intersections = 1000
    fudge_distance = 0.0001
    direction = (ray_destination - loc)
    dir_len = direction.length
    amount = fudge_distance / dir_len

    i = 1
    while (face_idx != -1):

        loc = loc.lerp(direction, amount)
        f = obj.ray_cast(mat * loc, mat * ray_destination)
        _, loc, normal, face_idx = f
        print(face_idx)
        if face_idx == -1:
            break
        i += 1
        if i > max_expected_intersections:
            break

    return not ((i % 2) == 0)



#bpy.ops.object.modifier_apply(apply_as='DATA', modifier=name)




#addVerticesInsideSelectionVolumeToVertexGroup(bpy.context.scene.objects["Icosphere"]
#                                    ,bpy.context.scene.objects["selector"],"crsm")


#bpy.context.scene.objects["Icosphere"]["smoothVertexGroup"]="crsm"

#addSmoothModifier(bpy.context.scene.objects["Icosphere"])


#addVerticesInsideSelectionVolumeToVertexGroup(bpy.context.scene.objects["Icosphere.001"]
#                                            ,bpy.context.scene.objects["selector"],[])








####
